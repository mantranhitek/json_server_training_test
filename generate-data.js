const { faker } = require("@faker-js/faker");
const { create } = require("domain");
const fs = require("fs");
const bcrypt = require("bcrypt");

// Func
const randomCatList = (params) => {
  if (params <= 0) return [];
  const catList = [];

  Array.from(new Array(params)).forEach(() => {
    const cat = {
      id: faker.string.uuid(),
      name: faker.animal.cat(),
      nickname: faker.person.firstName(),
      gender: faker.person.sexType(),
      age: faker.number.int({ min: 1, max: 6 }),
      isOwn: faker.datatype.boolean(),
      owner: null,
      image: faker.image.cats(),
      price: Number.parseFloat(faker.commerce.price({ min: 50, max: 300 })),
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };

    if (cat.isOwn) {
      cat.owner = faker.person.fullName();
    }

    catList.push(cat);
  });

  return catList;
};

const randomUserList = () => {
  const userList = [
    {
      id: faker.string.uuid(),
      username: "user1",
      password: "password1",
      nickname: faker.person.firstName(),
      email: faker.internet.email(),
      admin: faker.datatype.boolean(),
    },
    {
      id: faker.string.uuid(),
      username: "user2",
      password: "password2",
      nickname: faker.person.firstName(),
      email: faker.internet.email(),
      admin: faker.datatype.boolean(),
    },
  ];

  return userList;
};

// IIFE
(() => {
  // Random data
  const catList = randomCatList(30);
  const userList = randomUserList();

  // Prepare db object
  const db = {
    cats: catList,
    users: userList,
  };

  // Write db object to db.json
  fs.writeFile("db.json", JSON.stringify(db), () => {
    console.log("Generate data successfully");
  });
})();
