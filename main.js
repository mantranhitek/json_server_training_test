const jsonServer = require("json-server");
const queryString = require("node:querystring");
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const { faker } = require("@faker-js/faker");

const db = JSON.parse(fs.readFileSync("db.json", "utf8"));

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);

// Add custom routes before JSON Server router
server.get("/echo", (req, res) => {
  res.jsonp(req.query);
});

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  if (req.method === "POST") {
    req.body.createdAt = Date.now();
    req.body.updatedAt = Date.now();
  } else if (req.method === "PATCH") {
    req.body.updatedAt = Date.now();
  }
  // Continue to JSON Server router
  next();
});

router.render = (req, res) => {
  // Check GET with pagination
  // If yes, custom output

  const headers = res.getHeaders();

  const totalCountHeader = headers["x-total-count"];
  if (req.method === "GET" && totalCountHeader) {
    console.log(req.query);
    const queryParams = queryString.parse(req._parsedUrl.query);

    const result = {
      data: res.locals.data,
      pagination: {
        _page: Number.parseInt(queryParams._page) || 1,
        _limit: Number.parseInt(queryParams._limit) || 10,
        _totalRows: Number.parseInt(totalCountHeader),
      },
    };

    return res.jsonp(result);
  }

  // Otherwise, keep default behavior
  res.jsonp(res.locals.data);
};

// Login route
server.post("/api/login", (req, res) => {
  const { username, password } = req.body;

  // Tìm người dùng trong db.json dựa trên username
  const validate = db.users.find(
    (u) => u.username === username && u.password === password
  );

  if (!validate) {
    return res.status(401).json({ message: "Invalid username or password" });
  } else {
    const index = db?.users?.findIndex((item) => {
      return item?.username === username;
    });
    const result = {
      data: {
        id: db?.users[index]?.id,
        username: db?.users[index]?.username,
        nickname: db?.users[index]?.nickname,
        email: db?.users[index]?.email,
        admin: db?.users[index]?.admin,
      },
    };
    return res.jsonp(result);
  }
});

server.post("/api/register", (req, res) => {
  const { username, password, nickname, email, admin } = req.body;

  const usernameExists = db.users.some((u) => u.username === username);
  const emailExists = db.users.some((u) => u.email === email);

  if (usernameExists) {
    return res.status(400).json({ message: "Username already exists" });
  }

  if (emailExists) {
    return res.status(400).json({ message: "Email already exists" });
  }

  const newUser = {
    id: faker.string.uuid(),
    username,
    password,
    nickname,
    email,
    admin,
  };

  db.users.push(newUser);
  fs.writeFileSync("db.json", JSON.stringify(db));
  return res.jsonp({ message: "User created successfully", data: newUser });
});

// Use default router
server.use("/api", router);
server.listen(3000, () => {
  console.log("JSON Server is running");
});
